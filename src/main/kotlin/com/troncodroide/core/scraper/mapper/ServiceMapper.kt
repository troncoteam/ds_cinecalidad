package com.troncodroide.core.scraper.mapper


fun dec(data: String) = data.split(" ").map { it.toInt().minus(2).toChar() }.joinToString(separator = "")

object ServicesMapper {
    fun mapToServiceUrl(service: String, data: String): String {
        return when (service) {
            "OnlineUpToBox" -> "https://uptobox.com/" + dec(data)
            "OnlineYourUpload" -> "https://www.yourupload.com/watch/" + dec(data)
            "TheVideoMe" -> "https://thevideo.me/" + dec(data)
            "Vevio" -> "https://vev.io/" + dec(data)
            "OnlineFilesCDN" -> "https://filescdn.com/" + dec(data)
            "OnlineGD" -> "/protect/gdredirect.php?l=" + dec(data)
            "OnlineUsersCloud" -> "https://userscloud.com/" + dec(data)
            "OnlineUsersFiles" -> "https://usersfiles.com/" + dec(data)
            "OnlineOkRu" -> "https://ok.ru/video/" + dec(data)
            "OnlineOpenload" -> "https://openload.co/f/" + dec(data)
            "OnlineStreamango" -> "https://streamango.com/f/" + dec(data)
            "OnlineRapidVideo" -> "https://www.rapidvideo.com/v/" + dec(data)
            "OnlineMega" -> "https://mega.nz/#!" + dec(data)
            "UploadedTo" -> "http://uploaded.net/file/" + dec(data)
            "TurboBit" -> "https://turbobit.net/" + dec(data) + ".html"
            "1fichier" -> "https://1fichier.com/?" + dec(data) + "&af=2942360"
            "OnlineFembed" -> "https://www.fembeder.com/f/" + dec(data)
            "OnlineVerystream" -> "https://verystream.com/stream/" + dec(data)
            "OnlineGounlimited" -> "https://gounlimited.to/" + dec(data)
            "OnlineClipwatching" -> "https://clipwatching.com/" + dec(data)
            "OnlineVidcloud" -> "https://vidcloud.co/v/" + dec(data)
            "OnlineVidoza" -> "https://vidoza.net/" + dec(data) + ".html"
            "OnlineNetu" -> "https://netu.tv/watch_video.php?v=" + dec(data)
            "OnlineJetload" -> "https://jetload.net/p/" + dec(data)
            "OnlineOpenplay" -> "https://player.openplay.vip/player.php?id=" + dec(data)
            "Mega", "MediaFire" -> "/protect/v.php?i=" + dec(data)
            "Trailer" -> "https://www.youtube.com/watch?v=" + dec(data)
            else -> ""
        }
    }
}