package com.troncodroide.core.scraper.common
class Paginator(
    private val baseUrl: String,
    private val startPage: Int = 0,
    private val maxPages: Int = Int.MAX_VALUE,
    private val pageSufix: String = "/page/"
) {
    var currentPage = startPage
        get() {
            return field.coerceIn(0, maxPages)
        }

    fun getCurrentPage(): String = baseUrl + pageSufix + currentPage
    fun getNextPage(): String = baseUrl + pageSufix + ++currentPage
    fun reset() {
        currentPage = startPage
    }
}
