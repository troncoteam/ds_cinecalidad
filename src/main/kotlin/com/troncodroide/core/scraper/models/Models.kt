package com.troncodroide.core.scraper.models


data class Film(
    val title: String,
    val description: String? = null,
    val img: String? = null,
    val detailUrl: String? = null,
    val detail: Detail? = null
)

data class Detail(
    val links: List<ServiceLink> = mutableListOf()
)

data class ServiceLink(
    val name: String? = null,
    val data: String? = null,
    var link: String? = null
)
