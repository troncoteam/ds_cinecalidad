package com.troncodroide.core.scraper.datasource

import com.troncodroide.core.scraper.common.Paginator
import com.troncodroide.core.scraper.mapper.ServicesMapper
import com.troncodroide.core.scraper.models.Detail
import com.troncodroide.core.scraper.models.Film
import com.troncodroide.core.scraper.models.ServiceLink
import org.jsoup.Jsoup
import org.jsoup.nodes.Document


class FilmsDataSource() {
    val baseUrl = "https://www.cinecalidad.eu/espana"

    fun getPage(page: Int): List<Film> {
        val paginator = Paginator(baseUrl, startPage = page)
        val items = mutableListOf<Film>()
        val doc: Document = Jsoup.connect(paginator.getCurrentPage()).get()
        items.addAll(
            parseList(doc)
        )
        return items

    }

    fun getAll(numPages: Int): List<Film> {
        val paginator = Paginator(baseUrl)
        val items = mutableListOf<Film>()
        (0..numPages).forEach { _ ->
            val doc: Document = Jsoup.connect(paginator.getNextPage()).get()
            items.addAll(
                parseList(doc)
            )
        }
        return items
    }

    private fun parseList(doc: Document): List<Film> {
        return doc.select(".home_post_cont > a > img").map {
            Film(
                title = it.attr("title"),
                description = Jsoup.parse(it.attr("extract")).selectFirst(".home_post_content > p").text(),
                detailUrl = it.parent().attr("href"),
                img = it.attr("src")
            )
        }
    }

    fun getDetail(film: Film): Film {
        val doc: Document = Jsoup.connect(film.detailUrl).get()
        val elements = doc.select(".onlinelink").map {
            ServiceLink(
                name = it.attr("service"),
                data = it.attr("data")
            ).apply { link = ServicesMapper.mapToServiceUrl(name.orEmpty(), data.orEmpty()) }
        }
        return film.copy(detail = Detail(elements))
    }
}
