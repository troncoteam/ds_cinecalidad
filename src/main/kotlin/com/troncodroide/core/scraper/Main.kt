package com.troncodroide.core.scraper

import com.troncodroide.core.scraper.datasource.FilmsDataSource

fun main(args: Array<String>) {
    val items = FilmsDataSource().getPage(4)
    val detail = FilmsDataSource().getDetail(items.first())
    print(detail)
}
